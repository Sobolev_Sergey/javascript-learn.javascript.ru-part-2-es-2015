/****************************************************************************
* 8. СОВРЕМЕННЫЕ ВОЗМОЖНОСТИ ES-2015
****************************************************************************/

/*
1. ES-2015 СЕЙЧАС
*/
<!-- browser.js лежит на моём сервере, не надо брать с него -->
<script src="https://js.cx/babel-core/browser.min.js"></script>

<script type="text/babel">
  let arr = ["hello", 2]; // let

  let [str, times] = arr; // деструктуризация

  alert( str.repeat(times) ); // hellohello, метод repeat
</script>


/*
2. ПЕРЕМЕННЫЕ: LET И CONST
*/

var apples = 5;

if (true) {
  var apples = 10;

  alert(apples); // 10 (внутри блока)
}

alert(apples); // 10 (снаружи блока то же самое)

//*****************************
let apples = 5; // (*)

if (true) {
  let apples = 10;

  alert(apples); // 10 (внутри блока)
}

alert(apples); // 5 (снаружи блока значение не изменилось)

//*****************************
if (true) {
  let apples = 10;

  alert(apples); // 10 (внутри блока)
}

alert(apples); // ошибка!

//*****************************
function makeArmy() {

  let shooters = [];

  for (let i = 0; i < 10; i++) {
    shooters.push(function() {
      alert( i ); // выводит свой номер
    });
  }

  return shooters;
}

var army = makeArmy();

army[0](); // 0
army[5](); // 5

//*****************************
const user = {
  name: "Вася"
};

user.name = "Петя"; // допустимо
user = 5; // нельзя, будет ошибка


/*
3. ДЕСТРУКТУРИЗАЦИЯ
*/
'use strict';

let [firstName, lastName] = ["Илья", "Кантор"];

alert(firstName); // Илья
alert(lastName);  // Кантор

//*****************************
'use strict';

// первый и второй элементы не нужны
let [, , title] = "Юлий Цезарь Император Рима".split(" ");

alert(title); // Император

//*****************************
'use strict';

let [firstName, lastName, ...rest] = "Юлий Цезарь Император Рима".split(" ");

alert(firstName); // Юлий
alert(lastName);  // Цезарь
alert(rest);      // Император,Рима (массив из 2х элементов)

//*****************************
'use strict';

let [firstName, lastName] = [];

alert(firstName); // undefined

//*****************************
'use strict';

// значения по умолчанию
let [firstName="Гость", lastName="Анонимный"] = [];

alert(firstName); // Гость
alert(lastName);  // Анонимный

//*****************************
'use strict';

function defaultLastName() {
  return Date.now() + '-visitor';
}

// lastName получит значение, соответствующее текущей дате:
let [firstName, lastName=defaultLastName()] = ["Вася"];

alert(firstName); // Вася
alert(lastName);  // 1436...-visitor

//*****************************
'use strict';

let options = {
  title: "Меню",
  width: 100,
  height: 200
};

let {title, width, height} = options;

alert(title);  // Меню
alert(width);  // 100
alert(height); // 200

//*****************************
'use strict';

let options = {
  title: "Меню",
  width: 100,
  height: 200
};

let {width: w, height: h, title} = options;

alert(title);  // Меню
alert(w);      // 100
alert(h);      // 200

//*****************************
'use strict';

let options = {
  title: "Меню"
};

let {width=100, height=200, title} = options;

alert(title);  // Меню
alert(width);  // 100
alert(height); // 200

//*****************************
'use strict';

let options = {
  title: "Меню"
};

let {width:w=100, height:h=200, title} = options;

alert(title);  // Меню
alert(w);      // 100
alert(h);      // 200

//*****************************
'use strict';

let options = {
  title: "Меню",
  width: 100,
  height: 200
};

let {title, ...size} = options;

// title = "Меню"
// size = { width: 100, height: 200} (остаток)

//*****************************
'use strict';

let options = {
  size: {
    width: 100,
    height: 200
  },
  items: ["Пончик", "Пирожное"]
}

let { title="Меню", size: {width, height}, items: [item1, item2] } = options;

// Меню 100 200 Пончик Пирожное
alert(title);  // Меню
alert(width);  // 100
alert(height); // 200
alert(item1);  // Пончик
alert(item2);  // Пирожное


/*
4. ФУНКЦИИ
*/
function showMenu(title = "Без заголовка", width = 100, height = 200) {
  alert(title + ' ' + width + ' ' + height);
}

showMenu("Меню"); // Меню 100 200

//*****************************
function showMenu(title = "Заголовок", width = 100, height = 200) {
  alert('title=' + title + ' width=' + width + ' height=' + height);
}

// По умолчанию будут взяты 1 и 3 параметры
// title=Заголовок width=null height=200
showMenu(undefined, null);

//*****************************
function sayHi(who = getCurrentUser().toUpperCase()) {
  alert('Привет, ' + who);
}

function getCurrentUser() {
  return 'Вася';
}

sayHi(); // Привет, ВАСЯ

//*****************************
function showName(firstName, lastName, ...rest) {
  alert(firstName + ' ' + lastName + ' - ' + rest);
}

// выведет: Юлий Цезарь - Император,Рима
showName("Юлий", "Цезарь", "Император", "Рима");

//*****************************
//Если функция получает объект, то она может его тут же разбить в переменные:
'use strict';

let options = {
  title: "Меню"
};

function showMenu({title="Заголовок", width:w=100, height:h=200}) {
  alert(title + ' ' + w + ' ' + h);
}

// объект options будет разбит на переменные
showMenu(options); // Меню 100 200

//*****************************
//В свойстве name у функции находится её имя.
'use strict';

function f() {} // f.name == "f"

let g = function g() {}; // g.name == "g"

alert(f.name + ' ' + g.name) // f g

//*****************************
//Объявление функции Function Declaration, сделанное в блоке, видно только в 
//этом блоке.
 'use strict';

if (true) {

  sayHi(); // работает

  function sayHi() {
    alert("Привет!");
  }

}
sayHi(); // ошибка, функции не существует

//*****************************
//Появился новый синтаксис для задания функций через «стрелку» =>.
'use strict';

let inc = x => x+1;

alert( inc(1) ); // 2

//*****************************
let inc = x => x+1;

let inc = function(x) { return x + 1; };

//*****************************
'use strict';

let sum = (a,b) => a + b;

// аналог с function
// let sum = function(a, b) { return a + b; };

alert( sum(1, 2) ); // 3

//*****************************
//Если нужно задать функцию без аргументов, то также используются скобки, 
//в этом случае – пустые:
'use strict';

// вызов getTime() будет возвращать текущее время
let getTime = () => new Date().getHours() + ':' + new Date().getMinutes();

alert( getTime() ); // текущее время

//*****************************
//Когда тело функции достаточно большое, то можно его обернуть в фигурные 
//скобки {…}:
'use strict';

let getTime = () => {
  let date = new Date();
  let hours = date.getHours();
  let minutes = date.getMinutes();
  return hours + ':' + minutes;
};

alert( getTime() ); // текущее время

//*****************************
//Функции-стрелки очень удобны в качестве коллбеков, например:
`use strict`;

let arr = [5, 8, 3];

let sorted = arr.sort( (a,b) => a - b );

alert(sorted); // 3, 5, 8

//*****************************
//Функции-стрелки не имеют своего this
//Внутри функций-стрелок – тот же this, что и снаружи.
'use strict';

let group = {
  title: "Наш курс",
  students: ["Вася", "Петя", "Даша"],

  showList: function() {
    this.students.forEach(
      student => alert(this.title + ': ' + student)
    )
  }
}

group.showList();
// Наш курс: Вася
// Наш курс: Петя
// Наш курс: Даша

//*****************************
//Функции-стрелки не имеют своего arguments
'use strict';

function f() {
  let showArg = () => alert(arguments[0]);
  showArg();
}

f(1); // 1

//*****************************
'use strict';

function defer(f, ms) {
  return function() {
    setTimeout(() => f.apply(this, arguments), ms)
  }
}

function sayHi(who) {
  alert('Привет, ' + who);
}

let sayHiDeferred = defer(sayHi, 2000);
sayHiDeferred("Вася"); // Привет, Вася через 2 секунды

//*****************************
function defer(f, ms) {
  return function() {
    let args = arguments;
    let ctx = this;
    setTimeout(function() {
      return f.apply(ctx, args);
    }, ms);
  }
}


/*
5. СТРОКИ
*/
let str = `обратные кавычки`;
alert(`моя
  многострочная
  строка`);

'use strict';
let apples = 2;
let oranges = 3;

alert(`${apples} + ${oranges} = ${apples + oranges}`); // 2 + 3 = 5

//*****************************
'use strict';

function f(strings, ...values) {
  alert(JSON.stringify(strings));     // ["Sum of "," + "," =\n ","!"]
  alert(JSON.stringify(strings.raw)); // ["Sum of "," + "," =\\n ","!"]
  alert(JSON.stringify(values));      // [3,5,8]
}

let apples = 3;
let oranges = 5;

//          |  s[0] | v[0] |s[1]| v[1]  |s[2]  |      v[2]      |s[3]
let str = f`Sum of ${apples} + ${oranges} =\n ${apples + oranges}!`;

//*****************************
'use strict';

// str восстанавливает строку
function str(strings, ...values) {
  let str = "";
  for(let i=0; i<values.length; i++) {
    str += strings[i];
    str += values[i];
  }

  // последний кусок строки
  str += strings[strings.length-1];
  return str;
}

let apples = 3;
let oranges = 5;

// Sum of 3 + 5 = 8!
alert( str`Sum of ${apples} + ${oranges} = ${apples + oranges}!`);

//*****************************
'use strict';

let messages = {
  "Hello, {0}!": "Привет, {0}!"
};

function i18n(strings, ...values) {
  // По форме строки получим шаблон для поиска в messages
  // На месте каждого из значений будет его номер: {0}, {1}, …
  let pattern = "";
  for(let i=0; i<values.length; i++) {
    pattern += strings[i] + '{' + i + '}';
  }
  pattern += strings[strings.length-1];
  // Теперь pattern = "Hello, {0}!"

  let translated = messages[pattern]; // "Привет, {0}!"

  // Заменит в "Привет, {0}" цифры вида {num} на values[num]
  return translated.replace(/\{(\d)\}/g, (s, num) => values[num]);
}

// Пример использования
let name = "Вася";

// Перевести строку
alert( i18n`Hello, ${name}!` ); // Привет, Вася!


/*
6. ОБЪЕКТЫ И ПРОТОТИПЫ
*/
//Функция Object.assign получает список объектов и копирует в первый target 
//свойства из остальных.
'use strict';

let user = { name: "Вася" };
let visitor = { isAdmin: false, visits: true };
let admin = { isAdmin: true };

Object.assign(user, visitor, admin);

// user <- visitor <- admin
alert( JSON.stringify(user) ); // name: Вася, visits: true, isAdmin: true

//*****************************
//Вызов super.walk() из метода объекта rabbit обращается к animal.walk():
'use strict';

let animal = {
  walk() {
    alert("I'm walking");
  }
};

let rabbit = {
  __proto__: animal,
  walk() {
    alert(super.walk); // walk() { … }
    super.walk(); // I'm walking
  }
};

rabbit.walk();

//*****************************
//Исключением из этого правила являются функции-стрелки. В них используется 
//super внешней функции. Например, здесь функция-стрелка в setTimeout берёт 
//внешний super:
'use strict';

let animal = {
  walk() {
    alert("I'm walking");
  }
};

let rabbit = {
  __proto__: animal,
  walk() {
    setTimeout(() => super.walk()); // I'm walking
  }
};

rabbit.walk();


/*
7. КЛАССЫ
*/
'use strict';

class User {

  constructor(name) {
    this.name = name;
  }

  sayHi() {
    alert(this.name);
  }

}

let user = new User("Вася");
user.sayHi(); // Вася

//аналог
function User(name) {
  this.name = name;
}

User.prototype.sayHi = function() {
  alert(this.name);
};

//*****************************
//Геттеры, сеттеры и вычисляемые свойства
'use strict';

class User {
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  // геттер
  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }

  // сеттер
  set fullName(newValue) {
    [this.firstName, this.lastName] = newValue.split(' ');
  }

  // вычисляемое название метода
  ["test".toUpperCase()]() {
    alert("PASSED!");
  }

};

let user = new User("Вася", "Пупков");
alert( user.fullName ); // Вася Пупков
user.fullName = "Иван Петров";
alert( user.fullName ); // Иван Петров
user.TEST(); // PASSED!

//*****************************
//Статические свойства
'use strict';

class User {
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }
  static createGuest() {
    return new User("Гость", "Сайта");
  }
};
let user = User.createGuest();
alert( user.firstName ); // Гость
alert( User.createGuest ); // createGuest ... (функция)

//*****************************
//Наследование
'use strict';

class Animal {
  constructor(name) {
    this.name = name;
  }

  walk() {
    alert("I walk: " + this.name);
  }
}

class Rabbit extends Animal {
  walk() {
    super.walk();
    alert("...and jump!");
  }
}

new Rabbit("Вася").walk();
// I walk: Вася
// and jump!

//*****************************
'use strict';

class Animal {
  constructor(name) {
    this.name = name;
  }

  walk() {
    alert("I walk: " + this.name);
  }
}

class Rabbit extends Animal {
  constructor() {
    // вызвать конструктор Animal с аргументом "Кроль"
    super("Кроль"); // то же, что и Animal.call(this, "Кроль")
  }
}

new Rabbit().walk(); // I walk: Кроль

//*****************************
'use strict';

class Animal {
  constructor(name) {
    this.name = name;
  }
}

class Rabbit extends Animal {
  constructor() {
    alert(this); // ошибка, this не определён!
    // обязаны вызвать super() до обращения к this
    super();
    // а вот здесь уже можно использовать this
  }
}

new Rabbit();


/*
8. ТИП ДАННЫХ SYMBOL
*/
'use strict';

let sym = Symbol();
alert( typeof sym ); // symbol

//*****************************
'use strict';

let sym = Symbol("name");
alert( sym.toString() ); // Symbol(name)

//*****************************
//Существует «глобальный реестр» символов, который позволяет, при необходимости,
//иметь общие «глобальные» символы, которые можно получить из реестра по имени.
'use strict';

// создание символа в реестре
let name = Symbol.for("name");

// символ уже есть, чтение из реестра
alert( Symbol.for("name") == name ); // true

//*****************************
//Символы можно использовать в качестве имён для свойств объекта 
//следующим образом:
 'use strict';
let isAdmin = Symbol("isAdmin");
let user = {
  name: "Вася",
  [isAdmin]: true
};
alert(user[isAdmin]); // true

//*****************************
//Особенность символов в том, что если в объект записать свойство-символ, 
//то оно не участвует в итерации:

 'use strict';

let user = {
  name: "Вася",
  age: 30,
  [Symbol.for("isAdmin")]: true
};

// в цикле for..in также не будет символа
alert( Object.keys(user) ); // name, age

// доступ к свойству через глобальный символ — работает
alert( user[Symbol.for("isAdmin")] );

//*****************************


//*****************************


/*
9. ИТЕРАТОРЫ
*/
'use strict';

let arr = [1, 2, 3]; // массив — пример итерируемого объекта

for (let value of arr) {
  alert(value); // 1, затем 2, затем 3
}

//*****************************
//Также итерируемой является строка:
 'use strict';

for (let char of "Привет") {
  alert(char); // Выведет по одной букве: П, р, и, в, е, т

//*****************************
//Свой итератор
'use strict';

let range = {
  from: 1,
  to: 5
}

// сделаем объект range итерируемым
range[Symbol.iterator] = function() {

  let current = this.from;
  let last = this.to;

  // метод должен вернуть объект с методом next()
  return {
    next() {
      if (current <= last) {
        return {
          done: false,
          value: current++
        };
      } else {
        return {
          done: true
        };
      }
    }

  }
};

for (let num of range) {
  alert(num); // 1, затем 2, 3, 4, 5
}

//*****************************
//Если функционал по перебору (метод next) предоставляется самим объектом, то 
//можно вернуть this в качестве итератора:

'use strict';

let range = {
  from: 1,
  to: 5,

  [Symbol.iterator]() {
    return this;
  },

  next() {
    if (this.current === undefined) {
      // инициализация состояния итерации
      this.current = this.from;
    }

    if (this.current <= this.to) {
      return {
        done: false,
        value: this.current++
      };
    } else {
      // очистка текущей итерации
      delete this.current;
      return {
        done: true
      };
    }
  }

};

for (let num of range) {
  alert(num); // 1, затем 2, 3, 4, 5
}

// Произойдёт вызов Math.max(1,2,3,4,5);
alert( Math.max(...range) ); // 5 (*)

//*****************************
//Встроенные итераторы
'use strict';

let str = "Hello";

// Делает то же, что и
// for (var letter of str) alert(letter);

let iterator = str[Symbol.iterator]();

while(true) {
  let result = iterator.next();
  if (result.done) break;
  alert(result.value); // Выведет все буквы по очереди
}


/*
10. SET, MAP, WEAKSET И WEAKMAP
*/
//Map – коллекция для хранения записей вида ключ:значение.

'use strict';

let map = new Map();

map.set('1', 'str1');   // ключ-строка
map.set(1, 'num1');     // число
map.set(true, 'bool1'); // булевое значение

// в обычном объекте это было бы одно и то же,
// map сохраняет тип ключа
alert( map.get(1)   ); // 'num1'
alert( map.get('1') ); // 'str1'

alert( map.size ); // 3
//*****************************
//В качестве ключей map можно использовать и объекты:
'use strict';

let user = { name: "Вася" };

// для каждого пользователя будем хранить количество посещений
let visitsCountMap = new Map();

// объект user является ключом в visitsCountMap
visitsCountMap.set(user, 123);

alert( visitsCountMap.get(user) ); // 123

//*****************************
//Итерация
'use strict';

let recipeMap = new Map([
  ['огурцов',   '500 гр'],
  ['помидоров', '350 гр'],
  ['сметаны',   '50 гр']
]);

// цикл по ключам
for(let fruit of recipeMap.keys()) {
  alert(fruit); // огурцов, помидоров, сметаны
}

// цикл по значениям [ключ,значение]
for(let amount of recipeMap.values()) {
  alert(amount); // 500 гр, 350 гр, 50 гр
}

// цикл по записям
for(let entry of recipeMap) { // то же что и recipeMap.entries()
  alert(entry); // огурцов,500 гр , и т.д., массивы по 2 значения
}

//*****************************
//у Map есть стандартный метод forEach, аналогичный массиву:

 'use strict';

let recipeMap = new Map([
  ['огурцов',   '500 гр'],
  ['помидоров', '350 гр'],
  ['сметаны',   '50 гр']
]);

recipeMap.forEach( (value, key, map) => {
  alert(`${key}: ${value}`); // огурцов: 500 гр, и т.д.
});

//*****************************
//Set – коллекция для хранения множества значений, причём каждое значение может 
//встречаться лишь один раз.
'use strict';

let set = new Set();

let vasya = {name: "Вася"};
let petya = {name: "Петя"};
let dasha = {name: "Даша"};

// посещения, некоторые пользователи заходят много раз
set.add(vasya);
set.add(petya);
set.add(dasha);
set.add(vasya);
set.add(petya);

// set сохраняет только уникальные значения
alert( set.size ); // 3

set.forEach( user => alert(user.name ) ); // Вася, Петя, Даша

//*****************************
//Перебор Set осуществляется через forEach или for..of аналогично Map:

 'use strict';

let set = new Set(["апельсины", "яблоки", "бананы"]);

// то же, что: for(let value of set)
set.forEach((value, valueAgain, set) => {
  alert(value); // апельсины, затем яблоки, затем бананы
});

//*****************************
//WeakSet – особый вид Set не препятствующий сборщику мусора удалять свои 
//элементы. То же самое – WeakMap для Map.
// текущие активные пользователи
let activeUsers = [
  {name: "Вася"},
  {name: "Петя"},
  {name: "Маша"}
];

// вспомогательная информация о них,
// которая напрямую не входит в объект юзера,
// и потому хранится отдельно
let weakMap = new WeakMap();

weakMap.set(activeUsers[0], 1);
weakMap.set(activeUsers[1], 2);
weakMap.set(activeUsers[2], 3);
weakMap.set('Katya', 4); //Будет ошибка TypeError: "Katya" is not a non-null object

alert( weakMap.get(activeUsers[0]) ); // 1

activeUsers.splice(0, 1); // Вася более не активный пользователь

// weakMap теперь содержит только 2 элемента

activeUsers.splice(0, 1); // Петя более не активный пользователь

// weakMap теперь содержит только 1 элемент


/*
11. PROMISE
*/
'use strict';

// Создаётся объект promise
let promise = new Promise((resolve, reject) => {

  setTimeout(() => {
    // переведёт промис в состояние fulfilled с результатом "result"
    resolve("result");
  }, 1000);

});

// promise.then навешивает обработчики на успешный результат или ошибку
promise
  .then(
    result => {
      // первая функция-обработчик - запустится при вызове resolve
      alert("Fulfilled: " + result); // result - аргумент resolve
    },
    error => {
      // вторая функция - запустится при вызове reject
      alert("Rejected: " + error); // error - аргумент reject
    }
  );
  
//*****************************
// Этот promise завершится с ошибкой через 1 секунду
var promise = new Promise((resolve, reject) => {

  setTimeout(() => {
    reject(new Error("время вышло!"));
  }, 1000);

});

promise
  .then(
    result => alert("Fulfilled: " + result),
    error => alert("Rejected: " + error.message) // Rejected: время вышло!
  );

//*****************************
//Promise после reject/resolve – неизменны
'use strict';

let promise = new Promise((resolve, reject) => {

  // через 1 секунду готов результат: result
  setTimeout(() => resolve("result"), 1000);

  // через 2 секунды — reject с ошибкой, он будет проигнорирован
  setTimeout(() => reject(new Error("ignored")), 2000);

});

promise
  .then(
    result => alert("Fulfilled: " + result), // сработает
    error => alert("Rejected: " + error) // не сработает
  );

//*****************************
//А так – наоборот, ошибка будет раньше:

 'use strict';

let promise = new Promise((resolve, reject) => {

  // reject вызван раньше, resolve будет проигнорирован
  setTimeout(() => reject(new Error("error")), 1000);

  setTimeout(() => resolve("ignored"), 2000);

});

promise
  .then(
    result => alert("Fulfilled: " + result), // не сработает
    error => alert("Rejected: " + error) // сработает
  );

//*****************************
function httpGet(url) {

  return new Promise(function(resolve, reject) {

    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);

    xhr.onload = function() {
      if (this.status == 200) {
        resolve(this.response);
      } else {
        var error = new Error(this.statusText);
        error.code = this.status;
        reject(error);
      }
    };

    xhr.onerror = function() {
      reject(new Error("Network Error"));
    };

    xhr.send();
  });

}

//*****************************
//Цепочки промисов
/*«Чейнинг» (chaining), то есть возможность строить асинхронные цепочки из 
промисов – пожалуй, основная причина, из-за которой существуют и активно 
используются промисы.

Например, мы хотим по очереди:
- Загрузить данные посетителя с сервера (асинхронно).
- Затем отправить запрос о нём на github (асинхронно).
- Когда это будет готово, вывести его github-аватар на экран (асинхронно).
- …И сделать код расширяемым, чтобы цепочку можно было легко продолжить.
Вот код для этого, использующий функцию httpGet, описанную выше:
*/
'use strict';

// сделать запрос
httpGet('/article/promise/user.json')
  // 1. Получить данные о пользователе в JSON и передать дальше
  .then(response => {
    console.log(response);
    let user = JSON.parse(response);
    return user;
  })
  // 2. Получить информацию с github
  .then(user => {
    console.log(user);
    return httpGet(`https://api.github.com/users/${user.name}`);
  })
  // 3. Вывести аватар на 3 секунды (можно с анимацией)
  .then(githubUser => {
    console.log(githubUser);
    githubUser = JSON.parse(githubUser);

    let img = new Image();
    img.src = githubUser.avatar_url;
    img.className = "promise-avatar-example";
    document.body.appendChild(img);

    setTimeout(() => img.remove(), 3000); // (*)
  });
Самое главное в этом коде – последовательность вызовов:

httpGet(...)
  .then(...)
  .then(...)
  .then(...)

//*****************************
//Чтобы поймать всевозможные ошибки, которые возникнут при загрузке и обработке 
//данных, добавим catch в конец нашей цепочки:
'use strict';

// в httpGet обратимся к несуществующей странице
httpGet('/page-not-exists')
  .then(response => JSON.parse(response))
  .then(user => httpGet(`https://api.github.com/users/${user.name}`))
  .then(githubUser => {
    githubUser = JSON.parse(githubUser);

    let img = new Image();
    img.src = githubUser.avatar_url;
    img.className = "promise-avatar-example";
    document.body.appendChild(img);

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        img.remove();
        resolve();
      }, 3000);
    });
  })
  .catch(error => {
    alert(error); // Error: Not Found
  });

//*****************************
//Если всё хорошо, то then(user => httpGet(…)) вернёт новый промис, на который 
//стоят уже два обработчика:
httpGet('/article/promise/user.json')
  .then(JSON.parse)
  .then(user => httpGet(`https://api.github.com/users/${user.name}`))
  .then(
    JSON.parse,
    function avatarError(error) {
      if (error.code == 404) {
        return {name: "NoGithub", avatar_url: '/article/promise/anon.png'};
      } else {
        throw error;
      }
    }
  })

//*****************************
//Итого, после добавления оставшейся части цепочки, картина получаетсяследующей:

 'use strict';

httpGet('/article/promise/userNoGithub.json')
  .then(JSON.parse)
  .then(user => httpGet(`https://api.github.com/users/${user.name}`))
  .then(
    JSON.parse,
    function githubError(error) {
      if (error.code == 404) {
        return {name: "NoGithub", avatar_url: '/article/promise/anon.png'};
      } else {
        throw error;
      }
    }
  )
  .then(function showAvatar(githubUser) {
    let img = new Image();
    img.src = githubUser.avatar_url;
    img.className = "promise-avatar-example";
    document.body.appendChild(img);
    setTimeout(() => img.remove(), 3000);
  })
  .catch(function genericError(error) {
    alert(error); // Error: Not Found
  });

//*****************************
//Promise.all(iterable)
Promise.all([
  httpGet('/article/promise/user.json'),
  httpGet('/article/promise/guest.json'),
  httpGet('/article/promise/no-such-page.json') // (нет такой страницы)
]).then(
  result => alert("не сработает"),
  error => alert("Ошибка: " + error.message) // Ошибка: Not Found
)

//*****************************
//Promise.race(iterable)
Promise.race([
  httpGet('/article/promise/user.json'),
  httpGet('/article/promise/guest.json')
]).then(firstResult => {
  firstResult = JSON.parse(firstResult);
  alert( firstResult.name ); // iliakan или guest, смотря что загрузится раньше
});

//*****************************
//Promise.resolve(value)создаёт успешно выполнившийся промис с результатом value
Promise.resolve(window.location) // начать с этого значения
  .then(httpGet) // вызвать для него httpGet
  .then(alert) // и вывести результат
  
//*****************************
//Promise.resolve(value) создаёт уже выполнившийся промис, с ошибкой error
Promise.reject(new Error("..."))
  .catch(alert) // Error: ...

/*
Промисифицировать setTimeout

Напишите функцию delay(ms), которая возвращает промис, переходящий в состояние 
"resolved" через ms миллисекунд.
Пример использования:
*/
delay(1000)
  .then(() => alert("Hello!"))
//Такая функция полезна для использования в других промис-цепочках.
//Вот такой вызов:
return new Promise((resolve, reject) => {
  setTimeout(() => {
    doSomeThing();
    resolve();
  }, ms)
});
//Станет возможным переписать так:
return delay(ms).then(doSomething);

//РЕШЕНИЕ
function delay(ms) {
  return new Promise ((resolve, reject) => {
    setTimeout(resolve, ms);
  });
}


/*
Загрузить массив последовательно
Есть массив URL:
*/
 'use strict';

let urls = [
  'user.json',
  'guest.json'
];
/*
Напишите код, который все URL из этого массива загружает – один за другим 
(последовательно), и сохраняет в результаты в массиве results, а потом выводит.
Вариант с параллельной загрузкой выглядел бы так:
*/
Promise.all( urls.map(httpGet) )
  .then(alert);
//В этой задаче загрузку нужно реализовать последовательно.
// начало цепочки
let chain = Promise.resolve();

let results = [];

// в цикле добавляем задачи в цепочку
urls.forEach(function(url) {
  chain = chain
    .then(() => httpGet(url))
    .then((result) => {
      results.push(result);
    });
});

// в конце — выводим результаты
chain.then(() => {
  alert(results);
});


/*
12. ГЕНЕРАТОРЫ
*/
function* generateSequence() {
  yield 1;
  yield 2;
  return 3;
}
//*****************************
//Генератор – итератор
function* generateSequence() {
  yield 1;
  yield 2;
  return 3;
}

let generator = generateSequence();

for(let value of generator) {
  alert(value); // 1, затем 2
}

//*****************************
function* generateSequence() {
  yield 1;
  yield 2;
  yield 3;
}

let generator = generateSequence();

for(let value of generator) {
  alert(value); // 1, затем 2, затем 3
}
//*****************************
//Композиция генераторов
function* generateSequence(start, end) {
  for (let i = start; i <= end; i++) {
    yield i;
  }
}
// Используем оператор … для преобразования итерируемого объекта в массив
let sequence = [...generateSequence(2,5)];
alert(sequence); // 2, 3, 4, 5

//*****************************
function* generateSequence(start, end) {
  for (let i = start; i <= end; i++) yield i;
}

function* generateAlphaNum() {

  // 0..9
  yield* generateSequence(48, 57);

  // A..Z
  yield* generateSequence(65, 90);

  // a..z
  yield* generateSequence(97, 122);

}

let str = '';

for(let code of generateAlphaNum()) {
  str += String.fromCharCode(code);
}

alert(str); // 0..9A..Za..z

//*****************************
// генератор для получения и показа аватара
// он yield'ит промисы
function* showUserAvatar() {

  let userFetch = yield fetch('/article/generator/user.json');
  let userInfo = yield userFetch.json();

  let githubFetch = yield fetch(`https://api.github.com/users/
                                    ${userInfo.name}`);
  let githubUserInfo = yield githubFetch.json();

  let img = new Image();
  img.src = githubUserInfo.avatar_url;
  img.className = "promise-avatar-example";
  document.body.appendChild(img);

  yield new Promise(resolve => setTimeout(resolve, 3000));

  img.remove();

  return img.src;
}

// вспомогательная функция-чернорабочий
// для выполнения промисов из generator
function execute(generator, yieldValue) {

  let next = generator.next(yieldValue);

  if (!next.done) {
    next.value.then(
      result => execute(generator, result),
      err => generator.throw(err)
    );
  } else {
    // обработаем результат return из генератора
    // обычно здесь вызов callback или что-то в этом духе
    alert(next.value);
  }

}

execute( showUserAvatar() );

//*****************************
//Библиотека co, как и execute в примере выше, получает генератор и выполняет
//его.
co(function*() {
  let result = yield new Promise(
    resolve => setTimeout(resolve, 1000, 1)
  );
  alert(result); // 1
})

//*****************************
//Пример showUserAvatar() можно переписать с использованием co вот так:
 // Загрузить данные пользователя с нашего сервера
function* fetchUser(url) {
  let userFetch = yield fetch(url);
  let user = yield userFetch.json();

  return user;
}

// Загрузить профиль пользователя с github
function* fetchGithubUser(user) {
  let githubFetch = yield fetch(`https://api.github.com/users/${user.name}`);
  let githubUser = yield githubFetch.json();

  return githubUser;
}

// Подождать ms миллисекунд
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// Использовать функции выше для получения аватара пользователя
function* fetchAvatar(url) {
  let user = yield* fetchUser(url);
  let githubUser = yield* fetchGithubUser(user);

  return githubUser.avatar_url;
}

// Использовать функции выше для получения и показа аватара
function* showUserAvatar() {
  let avatarUrl;

  try {
    avatarUrl = yield* fetchAvatar('/article/generator/user.json');
  } catch(e) {
    avatarUrl = '/article/generator/anon.png';
  }

  let img = new Image();
  img.src = avatarUrl;
  img.className = "promise-avatar-example";
  document.body.appendChild(img);

  yield sleep(2000);

  img.remove();

  return img.src;
}
co(showUserAvatar);


/*
13. МОДУЛИ
*/
/*
Модулем считается файл с кодом.
В этом файле ключевым словом export помечаются переменные и функции, которые 
могут быть использованы снаружи.
Другие модули могут подключать их через вызов import.
*/
// экспорт прямо перед объявлением
export let one = 1;

// or
let two = 2;
export {two};

// or
export {one, two};

// or
export {one as once, two as twice};

// Экспорт функций и классов
export class User {
  constructor(name) {
    this.name = name;
  }
};

export function sayHi() {
  alert("Hello!");
};
// отдельно от объявлений было бы так:
// export {User, sayHi}

//IMPORT
import {one, two} from "./nums";

alert( `${one} and ${two}` ); // 1 and 2

// импорт one под именем item1, а two – под именем item2
import {one as item1, two as item2} from "./nums";

alert( `${item1} and ${item2}` ); // 1 and 2

//*****************************
//Например, файл user.js:
export default class User {
  constructor(name) {
    this.name = name;
  }
};
//…А в файле login.js:
import User from './user';
new User("Вася");


/*
14. PROXY
*/
//Прокси (proxy) – особый объект, смысл которого – перехватывать обращения к 
//другому объекту и, при необходимости, модифицировать их.
let proxy = new Proxy(target, handler)

//*****************************
'use strict';

let user = {};

let proxy = new Proxy(user, {
  get(target, prop) {
    alert(`Чтение ${prop}`);
    return target[prop];
  },
  set(target, prop, value) {
    alert(`Запись ${prop} ${value}`);
    target[prop] = value;
    return true;
  }
});

proxy.firstName = "Ilya"; // запись

proxy.firstName; // чтение

alert(user.firstName); // Ilya

//*****************************
'use strict';

let dictionary = {
  'Hello': 'Привет',
  'Bye': 'Пока'
};

alert( dictionary['Hello'] ); // Привет

//*****************************
'use strict';

let dictionary = {
  'Hello': 'Привет',
  'Bye': 'Пока'
};

dictionary = new Proxy(dictionary, {
  get(target, phrase) {
    if (phrase in target) {
      return target[phrase];
    } else {
      console.log(`No phrase: ${phrase}`);
      return phrase;
    }
  }
})

// Обращаемся к произвольным свойствам словаря!
alert( dictionary['Hello'] ); // Привет
alert( dictionary['Welcome']); // Welcome (без перевода)

//*****************************
//Ловушка has срабатывает в операторе in и некоторых других случаях, когда 
//проверяется наличие свойства.
'use strict';

let dictionary = {
  'Hello': 'Привет'
};

dictionary = new Proxy(dictionary, {
  has(target, phrase) {
    return true;
  }
});

alert("BlaBlaBla" in dictionary); // true

//*****************************
//Ловушка deleteProperty по синтаксису аналогична get/has.
'use strict';

let dictionary = {
  'Hello': 'Привет'
};

let proxy = new Proxy(dictionary, {
  deleteProperty(target, phrase) {
    return true; // ничего не делаем, но возвращает true
  }
});

// не удалит свойство
delete proxy['Hello'];

alert("Hello" in dictionary); // true

// будет то же самое, что и выше
// так как нет ловушки has, операция in сработает на исходном объекте
alert("Hello" in proxy); // true

//*****************************
//Ловушка enumerate перехватывает операции for..in и for..of по объекту.
'use strict';

let user = {
  name: "Ilya",
  surname: "Kantor",
  _version: 1,
  _secret: 123456
};

let proxy = new Proxy(user, {
  enumerate(target) {
    let props = Object.keys(target).filter(function(prop) {
      return prop[0] != '_';
    });

    return props[Symbol.iterator]();
  }
});

// отфильтрованы свойства, начинающиеся с _
for(let prop in proxy) {
  alert(prop); // Выведет свойства user: name, surname
}

//*****************************
/*
Метод apply(target, thisArgument, argumentsList) получает:
target – исходный объект.
thisArgument – контекст this вызова.
argumentsList – аргументы вызова в виде массива.
*/

'use strict';

function sum(a, b) {
  return a + b;
}

let proxy = new Proxy(sum, {
  // передаст вызов в target, предварительно сообщив о нём
  apply: function(target, thisArg, argumentsList) {
    alert(`Буду вычислять сумму: ${argumentsList}`);
    return target.apply(thisArg, argumentsList);
  }
});

// Выведет сначала сообщение из прокси,
// а затем уже сумму
alert( proxy(1, 2) );

//*****************************
//Ловушка construct(target, argumentsList) перехватывает вызовы при помощи new.
'use strict';

function User(name, surname) {
  this.name = name;
  this.surname = surname;
}

let UserProxy = new Proxy(User, {
  // передаст вызов new User, предварительно сообщив о нём
  construct: function(target, argumentsList) {
    alert(`Запуск new с аргументами: ${argumentsList}`);
    return new target(...argumentsList);
  }
});

let user = new UserProxy("Ilya", "Kantor");

alert( user.name ); // Ilya
